# catch-coding-challenge-1

- Fullname : Cokorda Gede Agung Yudi Dharma Putra
- Nickname : Coky
- Email    : cokagungyudi@gmail.com
- LinkedIn : linkedin.com/cokyudi

Thank you for having me to take this challenge,
To build this project I Use PHP Programming with Laravel Framework, and I use a mysql database to store the output of this program.
Based on my browse in internet, I still no idea about using AWS SDK to download the s3 file, so I just downloaded .jsonl file via http.
I use a CSV League library to convert and download the csv file because this library very simple and after checked in CSV Lint, the result is valid. For downloading the csv I created two ways,

First, according with the recommendations given to use Artisan Command. So we run the command that have been created after that the command will ask for the file name. I got a problem that the file can't be download but showing direct in command instead.

Second, downloading the file from the browser, So after run the artisan serve command and open the localhost:8000 in the browser the csv file will downloading directly, also the output will store to database in this moment. The stored output can expose using REST API and I created API to get all orders and API to get the order by order_id argument.

I tried to create XML output file but I got some error, so the feature isn't finished yet.

I will describe the steps to install and use the program below:

* STEP 1:
After cloning the project, in cmd go to the project repository and run "composer update" command, to download the library the used by this project.

* STEP 2:
If there isn't the ".env" file yet, copy the ".env.example" file and rename the file to ".env", and generate the .env key with command "php artisan key:generate".

* STEP 3:
Create a database with name "catch-coding-challenge-1", config your database connection to laravel in .env file (restart the web server every you make change in .env).

* STEP 4:
Run "php artisan migrate" command to add a table in database that have been created.

* STEP 5:
Run "php artisan download:csv" in command, input the file name, and the command will showing the output data.

* STEP 6:
Run "php artisan serve" to make the web service on, and the command will tell you the program running ini localhost:8000.

* STEP 7:
Open the browser and type "localhost:8000" in address bar, the output will directly downloding with csv format file. Use Ms. Excel to see the output csv format file. In this moment the output stored to database too.

* STEP 8:
To see the result of REST API just type "localhost:8000/orders" for all output, or "localhost:8000/order/{order_id}" for single output by order id. The browser will showing the REST API result in json format.


That's what I can do with the time and the questions given, I apologize if the task is not finished, I believe this is not my best yet and from this task i learn new things too like about jsonl file, AWS SDK, create self artisan command, and mostly about effectivy and time management to build the project.
Thank you very much.