<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function getAll()
    {
        $orders = Orders::get();

        echo json_encode($orders);
    }

    public function getOne($order_id)
    {
        $order = Orders::where('order_id',$order_id)->first();

        echo json_encode($order);
    }
}
