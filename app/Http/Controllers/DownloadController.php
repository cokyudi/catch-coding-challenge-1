<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Writer;
use Illuminate\Support\Facades\DB;
use Spatie\ArrayToXml\ArrayToXml;

class DownloadController extends Controller
{
    
    public function index()
    {
        $lines = file('https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl');

        $record = array();

        foreach($lines as $key=> $line)
        {
            $order = json_decode($line,true);
            
            $order_id = $order['order_id'];
            $record[$key]['order_id']=$order_id;

            $order_datetime = date('Y-m-h h:i:s',strtotime($order['order_date']));
            $record[$key]['order_date']=date(DATE_ISO8601,strtotime($order_datetime));

            $total = 0;
            $total_unit_price = 0;
            $total_unit = 0;
            foreach($order['items'] as $item)
            {
                $harga = $item['quantity']*$item['unit_price'];
                $total = $total + $harga;

                $total_unit_price = $total_unit_price + $item['unit_price'];

                $total_unit = $total_unit + $item['quantity'];
            }
            $disc = 0 ;
            foreach($order['discounts'] as $discount)
            {
                $disc = $disc+$discount['value'];
            }
            $record[$key]['total_order_value'] = $total-$disc;
            
            $record[$key]['average_unit_price'] = $total_unit_price/count($order['items']);

            $record[$key]['distict_unit_count'] = count($order['items']);

            $record[$key]['total_units_count'] = $total_unit;

            $record[$key]['customer_state'] = $order['customer']['shipping_address']['state'];
            
            DB::table('orders')->insert([
                'order_id'=>$record[$key]['order_id'],
                'order_datetime'=>date('Y-m-d h:i:s',strtotime($order_datetime)),
                'total_order_value'=>$record[$key]['total_order_value'],
                'average_unit_price'=>$record[$key]['average_unit_price'],
                'distinct_unit_count'=>$record[$key]['distict_unit_count'],
                'total_units_count'=>$record[$key]['total_units_count'],
                'customer_state'=>$record[$key]['customer_state']
            ]);
        }

        $header = ['order_id','order_datetime','total_order_value','average_unit_price','distinct_unit_count','total_units_count','customer_state'];
        $records = $record;

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne($header);
        $csv->insertAll($records);

        $csv->output('out.csv');

    }

    public function process($lines)
    {
        $record = array();

        foreach($lines as $key=> $line)
        {
            $order = json_decode($line,true);
            
            $order_id = $order['order_id'];
            $record[$key]['order_id']=$order_id;

            $order_datetime = date('Y-m-h h:i:s',strtotime($order['order_date']));
            $record[$key]['order_date']=date(DATE_ISO8601,strtotime($order_datetime));

            $total = 0;
            $total_unit_price = 0;
            $total_unit = 0;
            foreach($order['items'] as $item)
            {
                $harga = $item['quantity']*$item['unit_price'];
                $total = $total + $harga;

                $total_unit_price = $total_unit_price + $item['unit_price'];

                $total_unit = $total_unit + $item['quantity'];
            }
            $disc = 0 ;
            foreach($order['discounts'] as $discount)
            {
                $disc = $disc+$discount['value'];
            }
            $record[$key]['total_order_value'] = $total-$disc;
            
            $record[$key]['average_unit_price'] = $total_unit_price/count($order['items']);

            $record[$key]['distict_unit_count'] = count($order['items']);

            $record[$key]['total_units_count'] = $total_unit;

            $record[$key]['customer_state'] = $order['customer']['shipping_address']['state'];
            
        }

        return $record;
    }

    public function downloadCsv()
    {
        $lines = file('https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl');

        $records = $this->process($lines);

        $header = ['order_id','order_datetime','total_order_value','average_unit_price','distinct_unit_count','total_units_count','customer_state'];
        
        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne($header);
        $csv->insertAll($records);

        $csv->output('out.csv');

    }

    public function downloadXML()
    {
        $lines = file('https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl');
    
        $records['records'] = $this->process($lines);

        $result = ArrayToXml::convert($records);

    }
}
