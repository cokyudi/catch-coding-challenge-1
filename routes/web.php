<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'DownloadController@index');
Route::get('/download/csv', 'DownloadController@downloadCsv');
Route::get('/download/xml', 'DownloadController@downloadXML');

Route::get('/orders', 'OrdersController@getAll');
Route::get('/order/{order_id}', 'OrdersController@getOne');
